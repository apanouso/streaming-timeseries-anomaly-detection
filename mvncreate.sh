mvn archetype:generate \
    -DarchetypeGroupId=org.apache.flink \
    -DarchetypeArtifactId=flink-quickstart-java \
    -DarchetypeVersion=1.8.0 \
    -DgroupId=backend-test-project \
    -DartifactId=backend-test-project \
    -Dversion=0.1 \
    -Dpackage=backendtest \
    -DinteractiveMode=false
